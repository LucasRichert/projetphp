<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Insertion Film</title>
    <link rel="stylesheet" href="../css/Insertion.css">
  </head>
  <body>
    <?php
    try{
      include "Connexion.php";
      $connexion = connexion();
      if (!empty($_GET['nom_real'])){
        $result=$connexion->query('SELECT COUNT(*)+1 as nb, titre_francais from films');
        $id = $result['nb'];
        echo "$id";
        $nomReal = $_GET['nom_real'];
        $prenomReal = $_GET['prenom_real'];
        $res = $connexion->query('SELECT code_indiv from films natural join individus where realisateur=code_indiv and nom="$nomReal" and prenom = "$prenomReal"');
        insert_film($connexion,$_GET['titre_original'],$_GET['titre_francais'],$_GET['pays'],$_GET['date'],$_GET['duree'],$_GET['type'],$nomReal,$prenomReal);
      }
      if (!empty($_GET['nom_reals'])) {

      }
    }
    catch(PDOException $ex){
      echo $ex->getMessage();
    }

     ?>
     <h1>Insertion :</h1>
     <section >
       <h2>Film :</h2>
       <form class="insert_film" action="insertion.php" method="GET">
       Titre Original : <input type='text' name='titre_original' > </br>
       Titre Français : <input type='text' name='titre_francais' ></br>
       Pays : <input type='text' name='pays' ></br>
       Date : <input type='number' name='date' ></br>
       Durée : <input type='number' name='duree' ></br>
       Couleur:<select name='type'>
         <option value='couleur'>Couleur</option>
         <option value='NB'>Noir et Blanc</option>
       </select>
       </br>
       Nom Réalisateur :<input type='text' name='nom_reals' ></br>
       Prenom Réalisateur : <input type='text' name='prenom_reals' ></br>
       <input type="submit" value ="insert_film">
       </br>

     </section>
     <section >


       <h2>Réalisateur :</h2>
       <form class="insert_realisateur" action="insertion.php" method="GET">
         Nom Réalisateur : <input type='text' name='nom_real' ></br>
         Prenom Réalisateur : <input type='text' name='prenom_real' ></br>
         Nationalité : <input type='text' name='nat_real' ></br>
         Date de Naissance : <input type='text' name='dateNatReal' ></br>
         Date de Mort : <input type='text' name='dateMortReal' ></br>
         <input type="submit" value ="insert_realisateur">
       </form>
     </section>
  </body>
</html>
