<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Recherche</title>
    <link rel="stylesheet" href="../css/Film.css">
  </head>
  <body>
    <form name="recherche" method="GET" action="recherche.php">
      Rechercher : <input type="text" name="recherche">

      <select name="type">
      <option value="titre_original">Titre Original</option>
      <option value="titre_francais">Titre Français</option>
      <option value="pays">Pays</option>
      <option value="date">Date</option>
      <option value="duree">Durée</option>
      <option value="couleur">Couleur</option>
      <option value="nom">Nom Réalisateur</option>
      <option value="prenom">Prenom Réalisateur</option>
      <option value="nom_acteur">Nom Acteur</option>
      <option value="prenom_acteur">Prenom Acteur</option>
      <option value="nom_genre">Genre</option>
      </select>

      <input type="submit" value ="Recherche">
      <br/>
    </form>
      <?php
      include "Connexion.php";
      $connexion = connexion();
      if ((!empty($_GET['recherche']) and ($_GET['type']!=("nom_acteur" or "prenom_acteur" or "nom_genre")))){
        $rech = $_GET['recherche'];
        $typ = $_GET['type'];
        $result=$connexion->query("SELECT titre_original,titre_francais,pays,date,duree,couleur,nom,prenom from films natural join individus
          where $typ='$rech' and realisateur=code_indiv");
        echo "<h1>Films avec \'$rech\' :";
        echo "<table>\n";
        echo "<tr> <td>Titre Original</td><td>Titre Français</td><td>Pays</td><td>date</td><td>Durée</td><td>Couleur</td><td>Nom Real</td><td>Prenom Real</td>";
        foreach ($result as $film){
          echo "<tr>";
          echo "<td>".$film['titre_original']."</td>"."<td>".$film['titre_francais']."</td>"."<td>".$film['pays']."</td>"."<td>".$film['date']."</td>"."<td>".$film['duree'].
          "</td>"."<td>".$film['couleur']."</td>"."</td>"."<td>".$film['nom']."</td>"."<td>".$film['prenom']."</td>";
          echo "</tr>";
        }
        echo "</table>";
      }

      elseif (!empty($_GET['recherche']) and ($_GET['type'] != "nom_genre")) {
        if ($_GET['type']== "nom_acteur"){
          $typ = 'nom';
        }
        if ($_GET['type']== "prenom_acteur"){
          $typ = 'prenom';
        }
        $rech = $_GET['recherche'];
        echo "<h1>Les films avec $rech : </h1>";
        $result=$connexion->query("SELECT titre_original,titre_francais,pays,date,duree,couleur from films natural join acteurs natural join individus
          where ref_code_film=code_film and $typ='$rech' and  ref_code_acteur=code_indiv");
        echo "<table>\n";
        echo "<tr> <td>Titre Original</td><td>Titre Français</td><td>Pays</td><td>date</td><td>Durée</td><td>Couleur</td>";
        foreach ($result as $film){
          echo "<tr>";
          echo "<td>".$film['titre_original']."</td>"."<td>".$film['titre_francais']."</td>"."<td>".$film['pays']."</td>"."<td>".$film['date']."</td>"."<td>".$film['duree'].
          "</td>"."<td>".$film['couleur']."</td>"."</td>";
          echo "</tr>";
        }
        echo "</table>";
      }

      elseif (!empty($_GET['recherche']) and $_GET['type']=="nom_genre"){
        $rech = $_GET['recherche'];
        $typ = $_GET['type'];
        $result=$connexion->query("SELECT titre_original,titre_francais,pays,date,duree,couleur,nom,prenom from films natural join individus natural join classification natural join genres
          where $typ='$rech' and realisateur=code_indiv and ref_code_film=code_film and ref_code_genre=code_genre");

        echo "<h1>Films du genre $rech : </h1>";
        echo "<table>\n";
        echo "<tr> <td>Titre Original</td><td>Titre Français</td><td>Pays</td><td>date</td><td>Durée</td><td>Couleur</td><td>Nom Real</td><td>Prenom Real</td>";
        foreach ($result as $film){
          echo "<tr>";
          echo "<td>".$film['titre_original']."</td>"."<td>".$film['titre_francais']."</td>"."<td>".$film['pays']."</td>"."<td>".$film['date']."</td>"."<td>".$film['duree'].
          "</td>"."<td>".$film['couleur']."</td>"."</td>"."<td>".$film['nom']."</td>"."<td>".$film['prenom']."</td>";
          echo "</tr>";
        }
        echo "</table>";
      }

      elseif(empty($_GET['recherche'])){
        $result=$connexion->query('SELECT titre_original,titre_francais,pays,date,duree,couleur,nom,prenom from films natural join individus where realisateur=code_indiv and code_film<50');
        echo "<table>\n";
        echo "<tr> <td>Titre Original</td><td>Titre Français</td><td>Pays</td><td>date</td><td>Durée</td><td>Couleur</td><td>Nom Real</td><td>Prenom Real</td>";
        foreach ($result as $film){
          echo "<tr>";
          echo "<td>".$film['titre_original']."</td>"."<td>".$film['titre_francais']."</td>"."<td>".$film['pays']."</td>"."<td>".$film['date']."</td>"."<td>".$film['duree'].
          "</td>"."<td>".$film['couleur']."</td>"."</td>"."<td>".$film['nom']."</td>"."<td>".$film['prenom']."</td>";
          echo "</tr>";
        }
        echo "</table>";
      }
       ?>

       <form action="insertion.php" method="GET">
         <input type="submit" value ="insert">
       </form>
  </body>
</html>
