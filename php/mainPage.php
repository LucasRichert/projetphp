<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Liste des films</title>
    <link rel="stylesheet" href="../css/Film.css">
  </head>
  <body>
    <header>
      <h1>Liste des films</h1>
    </header>
    <article >
      <div>
        <form name="recherche" method="GET" action="recherche.php">
          Rechercher : <input type="text" name="recherche">

          <select name="type">
          <option value="titre_original">Titre Original</option>
          <option value="titre_francais">Titre Français</option>
          <option value="pays">Pays</option>
          <option value="date">Date</option>
          <option value="duree">Durée</option>
          <option value="couleur">Couleur</option>
          <option value="nom">Nom Réalisateur</option>
          <option value="prenom">Prenom Réalisateur</option>
          <option value="nom_acteur">Nom Acteur</option>
          <option value="prenom_acteur">Prenom Acteur</option>
          <option value="nom_genre">Genre</option>
          </select>

          <input type="submit" value ="Recherche">
        </form>
          <br/>
      </div>


    </article>

      <section>
        <?php
        try{
          include "Connexion.php";
          $connexion = connexion();
          $result=$connexion->query('SELECT titre_original,titre_francais,pays,date,duree,couleur,nom,prenom from films natural join individus where realisateur=code_indiv and code_film<50');
          echo "<table>\n";
          echo "<tr> <td>Titre Original</td><td>Titre Français</td><td>Pays</td><td>date</td><td>Durée</td><td>Couleur</td><td>Nom Real</td><td>Prenom Real</td>";
          foreach ($result as $film){
            echo "<tr>";
            echo "<td>".$film['titre_original']."</td>"."<td>".$film['titre_francais']."</td>"."<td>".$film['pays']."</td>"."<td>".$film['date']."</td>"."<td>".$film['duree'].
            "</td>"."<td>".$film['couleur']."</td>"."</td>"."<td>".$film['nom']."</td>"."<td>".$film['prenom']."</td>";
            echo "</tr>";
          }
          echo "</table>";
          $connexion=null;
        }
        catch(PDOException $ex){
          echo $ex->getMessage();
        }
         ?>
      </section>

     <form action="insertion.php" method="GET">
       <input type="submit" value ="insert">
     </form>
  </body>
</html>
